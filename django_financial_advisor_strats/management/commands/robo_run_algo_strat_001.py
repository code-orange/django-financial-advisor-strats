from decimal import Decimal
from time import sleep

from django.conf import settings
from django.core.management.base import BaseCommand

from django_financial_advisor_mdat.django_financial_advisor_mdat.models import *
from django_financial_advisor_strats.django_financial_advisor_strats.algo_strat_001 import *


class Command(BaseCommand):
    help = "Get Stock mean"

    def handle(self, *args, **options):
        # start bot and evaluate status every second
        trade_bot = AlgoSystem(settings.PROJ_GENERAL_ISIN, 250)

        stock_data = MdatStock.objects.get(symbol=settings.PROJ_GENERAL_ISIN)

        while True:
            stock_data.refresh_from_db()
            avg_stock = stock_data.get_mean_for_seconds(90)

            # pull stock data
            stock_last_price = stock_data.mdatstocklog_set.order_by("-tstamp")[0]

            data = {
                "bid_price": Decimal(stock_last_price.bid_price),
                "ask_price": Decimal(stock_last_price.ask_price),
                "average_bid_price": Decimal(avg_stock["bid_price"]["bid_price__avg"]),
                "average_ask_price": Decimal(avg_stock["ask_price"]["ask_price__avg"]),
            }

            trade_bot.tick_event(data)
            trade_bot.perform_trade_logic()

            # wait a second for next loop
            sleep(1)
