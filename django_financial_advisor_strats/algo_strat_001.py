import datetime as dt


class AlgoSystem:
    def __init__(self, symbol, order_volume_cap):
        self.order_volume_cap = order_volume_cap
        self.symbol = symbol
        self.bid_price = 0
        self.ask_price = 0
        self.average_bid_price = 0
        self.average_ask_price = 0

    def tick_event(self, data):
        self.bid_price = data["bid_price"]
        self.ask_price = data["ask_price"]
        self.average_bid_price = data["average_bid_price"]
        self.average_ask_price = data["average_ask_price"]

    def perform_trade_logic(self):
        # Is buying at the market lower than the average price?
        is_buy_signal = self.ask_price > self.average_ask_price

        # Is selling at the market higher than the average price?
        is_sell_signal = self.bid_price < self.average_bid_price

        # Print signal values on every tick
        print(
            dt.datetime.now(),
            " BUY/SELL? ",
            is_buy_signal,
            "/",
            is_sell_signal,
            " Avg (bid/ask):",
            self.average_bid_price,
            "/",
            self.average_ask_price,
        )

        # Use generated signals, if any, to open a position
        if (
            self.average_bid_price != 0
            and self.average_ask_price != 0
            and self.bid_price != 0
            and self.ask_price != 0
        ):
            if is_sell_signal:
                self.place_market_order(self.symbol, self.order_volume_cap, False)

            elif is_buy_signal:
                self.place_market_order(self.symbol, self.order_volume_cap, True)

    def place_market_order(self, symbol, order_volume_cap, is_buy):
        qty = order_volume_cap / self.ask_price

        if is_buy:
            keyword = "BUY"
        else:
            keyword = "SELL"

        print(
            "DEBUG: "
            + keyword
            + " "
            + str(qty)
            + "x "
            + symbol
            + "@"
            + str(self.ask_price)
        )

    def start(self):
        pass


if __name__ == "__main__":
    system = AlgoSystem("FB", 100, "30s", 5)
    system.start()
